# toki sona

A better toki pona dictionary. [Try it here.](http://tokisona.surge.sh)

### TODO

1. Prioritize exact word matches over all fuzzy matches.
2. Prioritize exact definition matches over fuzzy matches, but after word matches.
3. Prioritize fuzzy word matches over fuzzy definition matches.
4. Center the results in three columns per row.

### Want to use it?

```bash
$ git clone http://gitlab.com/docmenthol/tokisona.git
$ cd tokisona
$ yarn install
$ yarn start
```

### Want to build it?

```bash
$ yarn run build
```
