import React from 'react'
import { Input } from 'semantic-ui-react'

const Filter = ({ onFilterChange }) =>
  <Input
    size='huge'
    type='text'
    placeholder='Enter query...'
    onChange={onFilterChange}
    fluid
  />

export default Filter
