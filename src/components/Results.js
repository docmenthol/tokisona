import React from 'react'
import { Card } from 'semantic-ui-react'
import MediaQuery from 'react-responsive'
import { chunk as _chunk } from 'lodash'
import Word from './Word'

import Dictionary from '../dict/dict.json'

const Results = props => {
  let filter = props.filter.toLowerCase()
  let matches = Object.keys(Dictionary).filter(word =>
    word.indexOf(filter) > -1
      ? true
      : Object.keys(Dictionary[word])
          .some(mod => Dictionary[word][mod].join(' ').includes(filter)))
  let results = matches.map(word => ({ word, def: Dictionary[word] }))

  return (
    <div>
      {/* Desktop */}
      <MediaQuery query='(min-device-width: 1224px)'>
        { _chunk(results, 3).map((r, i) =>
          <Card.Group key={i} itemsPerRow={3}>
            { r.map((c, j) =>
              <Word key={j} word={c.word} def={c.def} query={props.filter} />
            )}
          </Card.Group>
        )}
      </MediaQuery>

      {/* Mobile */}
      <MediaQuery query='(max-device-width: 1224px)'>
        { _chunk(results, 2).map((r, i) =>
          <Card.Group key={i} itemsPerRow={2}>
            { r.map((c, j) =>
              <Word key={j} word={c.word} def={c.def} query={props.filter} />
            )}
          </Card.Group>
        )}
      </MediaQuery>
    </div>
  )
}

export default Results
