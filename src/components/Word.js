import React from 'react'
import { Card, Image } from 'semantic-ui-react'
import Highlighter from 'react-highlighter'
import PartsOfSpeech from '../dict/pos.json'

const colors = [
  'red', 'orange', 'yellow',
  'olive', 'green', 'teal',
  'blue', 'violet', 'purple',
  'pink', 'brown'
]

const Word = ({ word, def, query = '' }) =>
  <Card
    key={word}
    color={colors[Math.floor(Math.random() * colors.length)]}
    raised
  >
    <Image src={`img/words/${word}.jpg`} width={100} centered />
    <Card.Content>
      <Card.Header>
        <Highlighter
          search={query}
          matchStyle={{ backgroundColor: '#c5dbcb' }}
        >
          { word }
        </Highlighter>
      </Card.Header>
      <Card.Description>
        { Object.keys(def).map((m) => {
          return (
            <div key={m}>
              <em
                title={PartsOfSpeech[m]}
                style={{ borderBottom: '1px dotted black' }}
                >
                { m }
              </em>
              {' '}
              <Highlighter
                search={query}
                matchStyle={{ backgroundColor: '#c5dbcb' }}
                >
                { def[m].join(', ') }
              </Highlighter>
            </div>
          )
        })}
      </Card.Description>
    </Card.Content>
  </Card>

export default Word
