import React, { useState } from 'react'
import { Container, Grid, Header, Image } from 'semantic-ui-react'
import Filter from './components/Filter'
import Results from './components/Results'

const App = props => {
  let [ filter, setFilter ] = useState('')
  let updateInterval = null
  let handleFilterChange = e => {
    let f = e.target.value
    if (updateInterval) window.clearTimeout(updateInterval)
    updateInterval = window.setTimeout(() =>
      setFilter(f), 150)
  }

  return (
    <Container>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Header as='h2'>
              <Image src='img/logo.jpg' />
              <Header.Content>
                toki sona
                <Header.Subheader>a better toki pona word list</Header.Subheader>
                </Header.Content>
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Filter onFilterChange={handleFilterChange} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Results filter={filter} />
    </Container>
  )
}

export default App
